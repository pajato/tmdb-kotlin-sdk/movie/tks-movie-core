package com.pajato.tks.movie.core

import com.pajato.test.ReportingTestProfiler
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals

class CollectionUnitTest : ReportingTestProfiler() {
    @Test fun `When serializing a default movie collection, verify behavior`() {
        val collection = Collection()
        assertEquals("{}", Json.encodeToString(collection))
    }
}
