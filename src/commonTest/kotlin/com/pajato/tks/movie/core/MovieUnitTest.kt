package com.pajato.tks.movie.core

import com.pajato.test.ReportingTestProfiler
import com.pajato.tks.common.core.jsonFormat
import kotlinx.serialization.encodeToString
import kotlin.test.Test
import kotlin.test.assertEquals

class MovieUnitTest : ReportingTestProfiler() {
    @Test fun `When serializing a default movie, verify behavior`() {
        val movie = Movie()
        assertEquals("{}", jsonFormat.encodeToString(movie))
    }
}
