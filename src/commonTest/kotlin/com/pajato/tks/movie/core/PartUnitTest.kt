package com.pajato.tks

import com.pajato.test.ReportingTestProfiler
import com.pajato.tks.movie.core.Part
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals

class PartUnitTest : ReportingTestProfiler() {
    @Test fun `When serializing a default movie, verify behavior`() {
        val part = Part()
        assertEquals("{}", Json.encodeToString(part))
    }
}
