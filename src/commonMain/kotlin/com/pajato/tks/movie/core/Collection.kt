package com.pajato.tks.movie.core

import com.pajato.tks.common.core.BACKDROP_PATH
import com.pajato.tks.common.core.POSTER_PATH
import com.pajato.tks.common.core.TmdbId
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable public data class Collection(
    @SerialName(BACKDROP_PATH)
    val backdropPath: String = "",
    val id: TmdbId = 0,
    val name: String = "",
    val overview: String = "",
    @SerialName(POSTER_PATH)
    val posterPath: String = "",
    val parts: List<Part> = listOf(),
)
