package com.pajato.tks.movie.core

import com.pajato.tks.common.core.BACKDROP_PATH
import com.pajato.tks.common.core.POSTER_PATH
import com.pajato.tks.common.core.TmdbId
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable public data class Part(
    val adult: Boolean = false,
    @SerialName(BACKDROP_PATH)
    val backdropPath: String = "",
    @SerialName("genre_ids")
    val genreIds: List<Int> = listOf(),
    val id: TmdbId = 0,
    @SerialName("media_type")
    val mediaType: String = "",
    @SerialName("original_language")
    val originalLanguage: String = "",
    @SerialName("original_title")
    val originalTitle: String = "",
    val overview: String = "",
    val popularity: Double = 0.0,
    @SerialName(POSTER_PATH)
    val posterPath: String = "",
    @SerialName("release_date")
    val releaseDate: String = "",
    val title: String = "",
    val video: Boolean = false,
    @SerialName("vote_average")
    val voteAverage: Double = 0.0,
    @SerialName("vote_count")
    val voteCount: Int = 0,
)
