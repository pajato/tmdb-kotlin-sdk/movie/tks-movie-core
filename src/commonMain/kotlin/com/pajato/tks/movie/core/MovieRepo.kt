package com.pajato.tks.movie.core

import com.pajato.tks.common.core.MovieKey

public interface MovieRepo {
    public suspend fun getMovie(key: MovieKey): Movie
}
