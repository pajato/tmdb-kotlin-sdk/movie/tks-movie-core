package com.pajato.tks.movie.core

import com.pajato.tks.common.core.BACKDROP_PATH
import com.pajato.tks.common.core.Company
import com.pajato.tks.common.core.Country
import com.pajato.tks.common.core.Genre
import com.pajato.tks.common.core.POSTER_PATH
import com.pajato.tks.common.core.SpokenLanguage
import com.pajato.tks.common.core.TmdbId
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable public data class Movie(
    val adult: Boolean = false,
    @SerialName(BACKDROP_PATH)
    val backdropPath: String = "",
    @SerialName("belongs_to_collection")
    val belongsToCollection: Collection? = null,
    val budget: Int = 0,
    val genres: List<Genre> = listOf(),
    val homepage: String = "",
    val id: TmdbId = 0,
    @SerialName("imdb_id")
    val imdbId: String = "",
    @SerialName("original_language")
    val originalLanguage: String = "",
    @SerialName("original_title")
    val originalTitle: String = "",
    val overview: String = "",
    val popularity: Double = 0.0,
    @SerialName(POSTER_PATH)
    val posterPath: String = "",
    @SerialName("production_companies")
    val productionCompanies: List<Company> = listOf(),
    @SerialName("production_countries")
    val productionCountries: List<Country> = listOf(),
    @SerialName("release_date")
    val releaseDate: String = "",
    val revenue: Int = 0,
    val runtime: Int = 0,
    @SerialName("spoken_languages")
    val spokenLanguages: List<SpokenLanguage> = listOf(),
    val status: String = "",
    val tagline: String = "",
    val title: String = "",
    val video: Boolean = false,
    @SerialName("vote_average")
    val voteAverage: Double = 0.0,
    @SerialName("vote_count")
    val voteCount: Int = 0,
)
